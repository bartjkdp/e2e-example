import { Selector } from 'testcafe'

const getBaseUrl = require('../getBaseUrl.js')
const baseUrl = getBaseUrl();

fixture `Login page`
  .page `${baseUrl}`

test('The response from the API is visible in the UI', async t => {
  await t
    .expect(Selector('#root').innerText).eql('ok from API');
});
