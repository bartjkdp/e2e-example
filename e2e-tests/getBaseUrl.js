const getBaseUrl = () => {
    const url = process.env.URL || 'http://ui:8080/'

    if (!process.env.URL) {
      console.warn(`environment variable 'URL' not set. using the default ${url}`)
    }

    return url
  }

module.exports = getBaseUrl
