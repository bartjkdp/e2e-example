import React, { useState, useEffect } from 'react';

const App = () => {
  const [status, setStatus] = useState(null)

  useEffect(() => {
    fetch('/api').then((response) => {
      return response.json()
    })
    .then((data) => {
      setStatus(data.status)
    })
  })

  return (
    <div>{status ? status : 'Loading..'}</div>
  )
}

export default App
