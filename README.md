# End-to-end tests
This repository shows a setup for end-to-end tests with:

- React frontend
- Go backend
- Testcafe

## Running the tests

![image](/uploads/f79627a50cb4c5e37d0e72615d20b0f9/image.png)

```bash
docker-compose run \
-e BROWSERSTACK_USERNAME=$BROWSERSTACK_USERNAME \
-e BROWSERSTACK_ACCESS_KEY=$BROWSERSTACK_ACCESS_KEY \
e2e-tests
```

